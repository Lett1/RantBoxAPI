# RantBox, a REST API for Rant

Simple HTTP API to run Rant patterns

## Configuring

Open up config.json and set your stuff there:

```
{
  "hosturl": "http://localhost:8080", // URL to host on
  "pattern-timeout": 3.0,             // Pattern timeout
  "maxchars": 32768,                  // Maximum output length
  "package": "",                      // Path to rantpkg to load
  "hash-salt", "supersalty"			  // Optional, used as the salt for generating the ids for saved patterns
  "database", ""					  // Optional, If set, enables api for saving patterns. Uses c# connection strings.
}
```

Dont forget to grab the latest rantionary from https://github.com/RantLang/Rantionary

## Usage

Simply POST a json object to `/run` with the following data:

* `pattern`: The pattern to run.
* `includeHidden` (Optional): A hidden class to include. Set to nsfw for extra fun.

The server outputs the following json:

```
{
  statusType: "success",    // success, compiler-error, runtime-error, unknown-error
  statusMessage: "dfgh",    // Error messages go here
  seed: "123"               // The seed that was used
  output: {}                // Dictionary containing the indiviual channels and their outputs
}
```

## Database support

At the moment, only postgresql is supported. To enable database support, first create a table to hold the patterns:

```
CREATE TABLE rant_patterns (
	pattern_id serial PRIMARY KEY,
	pattern text NOT NULL
	);
```

Then, set the `database` and `hash-salt` settings to enable the endpoints:

* `GET  /load/{id}`: Loads a pattern for the given id.
* `POST /save`: Saves a pattern to the database and returns its id. 

To save a pattern, POST a json object with a `pattern` key to `/save`, on success the api responds with the id of the saved pattern.

```
$ curl -H "Content-Type: application/json" -X POST -d '{"pattern": "Hello, <noun>!"}' http://localst:8888/save ⏎
nV
```

To retrieve the pattern, simply GET `/load/{id}` with the id:

```
curl -H "Content-Type: application/json" -X GET http://localhost:8888/load/nV ⏎
{"pattern":"Hello, \u003cnoun\u003e!"}
```
