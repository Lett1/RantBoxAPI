﻿using System.Collections.Generic;
using System.IO;

namespace RantBoxNeo
{
    public class Settings : ISettings
    {
        public Dictionary<string, object> LoadSettings()
        {
            var reader = new StreamReader("config.json");
            var data = reader.ReadToEnd();
            return (Dictionary<string, object>)data.FromJson<object>();
        }

        public static Dictionary<string, object> StaticLoadSettings()
        {
            var reader = new StreamReader("config.json");
            var data = reader.ReadToEnd();
            return (Dictionary<string, object>)data.FromJson<object>();
        }
    }
}