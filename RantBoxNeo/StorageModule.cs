﻿using System.Collections.Generic;
using System;
using Nancy;
using Nancy.ModelBinding;
using Npgsql;
using HashidsNet;

namespace RantBoxNeo
{
    public class SaveModel
    {
        public string Pattern { get; set; }

        public SaveModel() {}

        public SaveModel(string pattern)
        {
            this.Pattern = pattern;
        }
    }

    public class StorageModule : NancyModule
    {
        Dictionary<string, object> settings;
        Hashids hashIds;
        string connString; 
        
        public StorageModule(ISettings settings)
        {
            this.settings = settings.LoadSettings();
            this.hashIds = new Hashids((string)this.settings["hash-salt"]);

            this.connString = (string)this.settings["database"];

            Before += ctx =>
            {
                return null;
            };
            
            Post["/save"] = SavePattern;
            Get["/load/{pattern_hash:maxlength(20)}"] = LoadPattern;

            After += ctx =>
            {

            };

        }

        dynamic SavePattern(dynamic parameters)
        {
            var data = this.Bind<SaveModel>();

			var conn = new NpgsqlConnection(connString);
			try
			{
				conn.Open();

				var cmd = new NpgsqlCommand("INSERT INTO rant_patterns VALUES (DEFAULT, @p) RETURNING pattern_id;", conn);
				try
				{
                    cmd.Parameters.AddWithValue("p", data.Pattern);
                    Object new_id = cmd.ExecuteScalar();
					
                    if (new_id != null)
					{
                        return this.hashIds.Encode((Int32)new_id);
					}
					else
					{
                        return HttpStatusCode.InternalServerError;
					}
					
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
				}
				finally
				{
					if (cmd != null)
						cmd.Dispose();
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
			finally
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}
			}

            return HttpStatusCode.InternalServerError;
        }

        dynamic LoadPattern(dynamic parameters)
        {
            //Decode returns an empty array if the string could not be decoded, check for that
            int[] pattern_id_decode = this.hashIds.Decode((string)parameters.pattern_hash);
            int pattern_id;

            if(pattern_id_decode.Length > 0)
            {
                pattern_id = pattern_id_decode[0];
            }
            else
            {
                return HttpStatusCode.NotFound;
            }

            var conn = new NpgsqlConnection(this.connString);
            try
			{
                conn.Open();

				var cmd = new NpgsqlCommand("SELECT * FROM rant_patterns WHERE pattern_id = @p", conn);
				try
                {
                    cmd.Parameters.AddWithValue("p", pattern_id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var pattern = reader.GetString(1);
                                return Negotiate.WithModel(new SaveModel(pattern));
                            }
                        }
                        else
                        {
                            return HttpStatusCode.NotFound;
                        }
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    if (cmd != null)
                        cmd.Dispose();
                }
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
			finally
			{
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
			}

            return HttpStatusCode.InternalServerError;
            
        }
        
    }
}