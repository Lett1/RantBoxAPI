﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;
using Rant;

namespace RantBoxNeo
{
    public class QueryModel
    {
        public string Pattern { get; set; }

        public string IncludeHidden { get; set; }
    }

    public class ResponseModel
    {
        public ResponseModel(string statusType, string statusMessage, long seed, Dictionary<string, string> output)
        {
            StatusType = statusType;
            StatusMessage = statusMessage;
            Seed = seed;
            Output = output;
        }

        public string StatusType { get; set; }

        public string StatusMessage { get; set; }

        public long Seed { get; set; }

        public Dictionary<string, string> Output { get; set; }
    }

    public class RantBoxRoute : NancyModule
    {
        RantEngine rant;
        Dictionary<string, object> settings;

        public RantBoxRoute(ISettings settings)
        {
            this.settings = settings.LoadSettings();

            Post["/run"] = DoQuery;

            rant = new RantEngine();
            rant.LoadPackage((string)this.settings["package"]);
        }

        dynamic DoQuery (dynamic parameters)
        {
            var test = this.Bind<QueryModel>();

            if (!string.IsNullOrEmpty(test.Pattern))
            {
                if (!string.IsNullOrEmpty(test.IncludeHidden))
                {
                    rant.Dictionary.IncludeHiddenClass(test.IncludeHidden);
                }

                string statusType;
                string statusMessage = null;
                long seed = 0;
                Dictionary<string, string> output = null;
                int statusCode = 500;

                try
                {
                    var pgm = RantProgram.CompileString(test.Pattern);
                    // Run the program
                    var rantOutput = rant.Do(pgm, (int) settings["maxchars"], (double) settings["pattern-timeout"]);
                    seed = rantOutput.Seed;
                    output = rantOutput.Where(x => x.Value != "").ToDictionary(x => x.Name, x => x.Value);
                    statusType = "success";
                    statusCode = 200;

                }
                catch (RantCompilerException e)
                {
                    Console.WriteLine(e);
                    statusType = "compiler-error";
                    statusMessage = e.Message;
                    statusCode = 500;
                }
                catch (RantRuntimeException e)
                {
                    Console.WriteLine(e);
                    statusType = "runtime-error";
                    statusMessage = e.Message;
                    statusCode = 500;
                }

                return Negotiate.WithModel(new ResponseModel(statusType, statusMessage, seed, output))
                    .WithStatusCode(statusCode);
            }

            rant.Dictionary.ExcludeHiddenClass(test.IncludeHidden);

            return new TextResponse(HttpStatusCode.BadRequest, "No pattern supplied");
        }

    }
}