﻿using System.Collections.Generic;

namespace RantBoxNeo
{
    public interface ISettings
    {
         Dictionary<string, object> LoadSettings();
    }
}