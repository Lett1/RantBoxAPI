﻿using System.Collections.Generic;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Diagnostics;
using Nancy.Responses.Negotiation;
using Nancy.TinyIoc;

using System.Linq;

namespace RantBoxNeo
{
    public class NancyStrapper : DefaultNancyBootstrapper
    {

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            //base.ConfigureApplicationContainer(container);
            container.Register<ISettings>(new Settings());

        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            container.Register<IDiagnostics, DisabledDiagnostics>();

            pipelines.AfterRequest += ctx =>
            {
                ctx.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                ctx.Response.Headers.Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
                ctx.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST");
            };
        }

        //Only return json respones
		protected override NancyInternalConfiguration InternalConfiguration
		{
			get
			{
				return NancyInternalConfiguration.WithOverrides(
					(c) =>
					{
						c.ResponseProcessors.Clear();
						c.ResponseProcessors.Add(typeof(JsonProcessor));
					});
			}
		}

		protected override IEnumerable<ModuleRegistration> Modules
		{
			get
			{
                var modules = base.Modules;
                var settings = Settings.StaticLoadSettings();
                if(!settings.ContainsKey("database") || (string)settings["database"] == "")
                {
                    return modules.Where(m => m.ModuleType != typeof(StorageModule)).ToArray();
                }

                return modules;
			}
		}
    }
}